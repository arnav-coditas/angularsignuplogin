import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ILogin } from '../Login';
import { ISignup } from '../Signup';

@Injectable({
  
  providedIn: 'root',
})
export class DataService {
  headers = new HttpHeaders().set('ngrok-skip-browser-warning', '1234');

  constructor(private http: HttpClient) {}
  getUser(data:any) {
    return this.http.get(
      `https://9707-1-22-124-182.ngrok.io/user/getUser/${data.id}`,
      {
        headers: {
          Authorization: `Bearer ${data.jwt}`,
          'ngrok-skip-browser-warning': '1234',
        },
      }
    );
  }
  postLoginDetails(data: ILogin) {
    return this.http.post(
      'https://9707-1-22-124-182.ngrok.io/login',
      data,
      { headers: this.headers }
    );
  }
  postSigup(data: ISignup) {
    return this.http.post(
      'https://9707-1-22-124-182.ngrok.io/register',
      data,
      { headers: this.headers }
    );
  }
}
