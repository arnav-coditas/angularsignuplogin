import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ILogin} from '../Login';

import { DataService } from '../services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor() {
  }
  @Output() sendevent = new EventEmitter();

   onSubmitform(f:NgForm){
    let data: ILogin = { userEmail: f.value.email, password: f.value.password };
 
    this.sendevent.emit(data)
    }

   
 
  ngOnInit(): void {
   
  }
  


 
}

