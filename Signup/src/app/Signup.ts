export interface ISignup{
   
    userName?:string,
    userEmail?:string,
    password?:string,
    userRole?:string
    userSalary?:string,
    userCity?:string,
    jwtToken?:string,
    jwtId?:string,
}