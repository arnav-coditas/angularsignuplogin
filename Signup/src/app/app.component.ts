import { Component } from '@angular/core';
import { Data } from '@angular/router';
import { ILogin } from './Login';
import { DataService } from './services/data.service';
import { ISignup } from './Signup';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Signup';
  LoginObject!: ILogin;
  SignupObject!: ISignup;
  userDetails !: ISignup;
  token!: '';
  Id!: '';

  data = { jwt: this.token, id: this.Id };
  constructor(private DataService: DataService) {}

  sendLoginDetails(e: ILogin) {
    this.LoginObject = e;
    this.DataService.postLoginDetails(this.LoginObject).subscribe({
      next: (response: any) => {
        this.token = response.jwtToken;
        this.Id = response.jwtId;
        this.data.jwt = this.token;
        this.data.id = this.Id;
      },
      complete: () => {
        this.getUserData(this.data);
      },
    });
  }

  sendSignupDetails(e: ISignup) {
    this.SignupObject = e;
    this.DataService.postSigup(this.SignupObject).subscribe((result) => {
      console.log(result);
    
    });
  }
  
  getUserData(data: {}) {
    console.log(this.token);
    this.DataService.getUser(data).subscribe((result) => {
      this.userDetails = result;
    });
  }


}

