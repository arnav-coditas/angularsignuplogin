import { NgFor } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

import { DataService } from '../services/data.service';
import { ISignup } from '../Signup';




@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})

export class SignupComponent  {
  @Output() sendSignupData = new EventEmitter();
 constructor(private DataService: DataService){}



  onSubmitSignupform(f:NgForm){
  
  let data: ISignup= {password:f.value.password, userEmail: f.value.email ,userCity:f.value.city,userName: f.value.name, userRole:f.value.role,userSalary:f.value.salary};
  this.sendSignupData.emit(data)
  

     }



}


